import React, { Component } from 'react';
import './App.css';
import './bootstrap.min.css';
import boot from './boot.png';
import gambar from './gambar.png';
class App extends Component {
  render() {
    return (
      <div className="App">
        {/* -----------navbar-------- */}
        <header className="navbar flex-column navbar-dark">
          <nav className="navbar fixed-top navbar-expand  ">
            <a className="navbar-brand mr-0 mr-md-2" href="/" aria-label="Bootstrap"><svg className="d-block" width="36" height="36" viewBox="0 0 612 612" xmlns="http://www.w3.org/2000/svg" focusable="false"><title>Bootstrap</title><path fill="currentColor" d="M510 8a94.3 94.3 0 0 1 94 94v408a94.3 94.3 0 0 1-94 94H102a94.3 94.3 0 0 1-94-94V102a94.3 94.3 0 0 1 94-94h408m0-8H102C45.9 0 0 45.9 0 102v408c0 56.1 45.9 102 102 102h408c56.1 0 102-45.9 102-102V102C612 45.9 566.1 0 510 0z"></path><path fill="currentColor" d="M196.77 471.5V154.43h124.15c54.27 0 91 31.64 91 79.1 0 33-24.17 63.72-54.71 69.21v1.76c43.07 5.49 70.75 35.82 70.75 78 0 55.81-40 89-107.45 89zm39.55-180.4h63.28c46.8 0 72.29-18.68 72.29-53 0-31.42-21.53-48.78-60-48.78h-75.57zm78.22 145.46c47.68 0 72.73-19.34 72.73-56s-25.93-55.37-76.46-55.37h-74.49v111.4z"></path></svg></a>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav mr-auto">
                <li className="nav-item active">
                  <a className="nav-link" href="#">Home <span className="sr-only">(current)</span></a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Documentation</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Examples</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Themes</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Expo</a>
                </li>
                <li className="nav-item">
                  <a className="nav-link" href="#">Download</a>
                </li>
              </ul>
              <form className="nav-item form-inline my-2 my-lg-0">
                <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">v4.1</a>
                <a className="btn btn-outline-warning btn-bd-download d-none d-lg-inline-block mb-3 mb-md-0 ml-md-3" href="">Download</a>
              </form>
            </div>
          </nav>
        </header>
        {/* -----------akhir navbar-------- */}



        {/* -----------isi-------- */}
        <div className="container contener">
          <div className="row">
            <div className="col-6">
              <h1>Bootstrap</h1>
              <p className="lead">
                Build responsive, mobile-first projects on the web with<br/>
                the world's most popular front-end component library.
              </p>
              <p className="lead">
                  Bootstrap is an open source toolkit for developing with<br/>
                  HTML, CSS, and JS. Quickly prototype your ideas or<br/>
                  build your entire app with our Sass variables and mixins,<br/>
                  responsive grid system, extensive prebuilt components,<br/>
                  and powerful plugins built on jQuery.
              </p>
              <nav className="navbar navbar-light bg-light">
                  <form className="form-inline">
                    <a className="btn btn-outline-info btn-bd-download d-none d-lg-inline-block mb-3 mb-md-0 ml-md-3" id="btn" href="">Get started</a>
                    <a className="btn btn-outline-secondary btn-bd-download d-none d-lg-inline-block mb-3 mb-md-0 ml-md-3" id="btn" href="">Download</a>
                  </form>
              </nav>
              <p className="text-muted">Currently v4.1.3</p>
            </div>
            <div className="col-6">
              <img src={boot} height="450" width="550"/>
            </div>
          </div>
        </div><br/><br/><br/><br/><br/><br/><br/><br/><br/>
        {/* -----------akhir isi-------- */}


        {/* -----------dokumentasi-------- */}
        <div className="masthead-followup row m-0 border border-white">
            <div className="col-12 col-md-4 p-3 p-md-5 bg-light border border-white">
              <svg xmlns="http://www.w3.org/2000/svg" focusable="false" width="32" height="32" fill="none" stroke="currentcolor" stroke-width="2" className="text-primary mb-2" viewBox="0 0 32 32" stroke-linecap="round" stroke-linejoin="round"><title>Import icon</title><path d="M28 22v8H4v-8M16 4v20m-8-8l8 8 8-8"></path></svg>  
              <div><p>Installation</p></div>
                <p>
                    Include Bootstrap's source Sass and JavaScript<br/>
                    files via npm, Composer or Meteor. Package<br/>
                    managed installs don't include documentation,<br/>
                    but do include our build system and readme.<br/>
                    <br/>    
                </p>
                <p>$ npm install bootstrap</p>
                <p>$ gem install bootstrap -v 4.1.3</p><br/><br/>
                <button className="btn btn-outline-primary" href="" type="button">Read installation docs</button>
            </div>
            <div className="col-12 col-md-4 p-3 p-md-5 bg-light border border-white">
                <svg xmlns="http://www.w3.org/2000/svg" focusable="false" width="32" height="32" fill="none" stroke="currentcolor" stroke-width="2" className="text-primary mb-2" viewBox="0 0 32 32" stroke-linecap="round" stroke-linejoin="round"><title>Download icon</title><path d="M9 22c-9 1-8-10 0-9C6 2 23 2 22 10c10-3 10 13 1 12m-12 4l5 4 5-4m-5-10v14"></path></svg>
                <div><p>BootstrapCDN</p></div>
                <p>
                    When you only need to include Bootstrap's<br/>
                    compiled CSS or JS, you can use BootstrapCDN.
                </p>
                <p>CSS only</p>
                <p>JS, Popper.js, and jQuery</p><br/><br/>
                <button className="btn btn-outline-primary" href="" type="button">Explore the docs</button>
            </div>
            <div className="col-12 col-md-4 p-3 p-md-5 bg-light border border-white">
                <svg xmlns="http://www.w3.org/2000/svg" focusable="false" width="32" height="32" fill="none" stroke="currentcolor" stroke-width="2" className="text-primary mb-2" viewBox="0 0 32 32" stroke-linecap="round" stroke-linejoin="round"><title>Lightning icon</title><path d="M18 13l8-11L8 13l6 6-8 11 18-11z"></path></svg>
                <div><p>Official Themes</p></div>
                <p>
                    Take Bootstrap 4 to the next level with official<br/>
                    premium themes—toolkits built on Bootstrap with<br/>
                    new components and plugins, docs, and build<br/>
                    tools. 
                </p>
                <div className="gambar"><img  src={gambar} height="150" width="370"/></div><br/><br/>
                <button className="btn btn-outline-primary" href="" type="button">Browse themes</button>
            </div>
        </div>
        {/* -----------akhir dokumentasi-------- */}


        {/* -----------footer-------- */}
        <footer class="bd-footer text-muted">
            <div class="container-fluid p-3 p-md-5">
              <a href="" class="q">GitHub</a>&nbsp;&nbsp;
              <a href="" class="q">Twitter</a>&nbsp;&nbsp;
              <a href="" class="q">Examples</a>&nbsp;&nbsp;
              <a href="" class="q">About</a><br/><br/>
              <p>Designed and built with all the love in the world by <a href="" class="q">@mdo</a> and <a href="" class="q">@fat</a>. Maintained by the <a href="" class="q">core team</a> with the help of <a href="" class="q">our contributors</a>.<br/>
              Currently v4.1.3. Code licensed <a href="" class="q">MIT</a>, docs <a href="" class="q">CC BY 3.0</a>.</p>
            </div>
        </footer>
        {/* -----------akhir footer-------- */}
      </div>
    );
  }
}

export default App;
