import React, { Component } from 'react';
import './bootstrap.min.css';
import './main.css';
import profile from './img/fatih.jpg'
import bromo from './img/bromo.jpg'
import gambar from './img/gambar.jpg'
import pantai from './img/pantai.jpg'
import hp from './img/hp.jpg'
import fatih from './img/fatih.jpg'
import job from './img/job.jpg'

class Porto extends Component {
  render() {
    return (
      <div className="Appp">
        {/* -----------navbar-------- */}
        <nav className="navbar navbar-expand-lg navbar-dark  fixed-top nabar">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo03" aria-controls="navbarTogglerDemo03" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <a className="navbar-brand scroll" href="#jumbo">Ibrahim Alfatih</a>
        
          <div className="collapse navbar-collapse" id="navbarTogglerDemo03">
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item active">
                <a className="nav-link scroll" href="#about">About Me<span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item active">
                <a className="nav-link scroll" href="#gallery">Gallery<span className="sr-only">(current)</span></a>
              </li>
              <li className="nav-item active">
                <a className="nav-link scroll" href="#contact">Contact<span className="sr-only">(current)</span></a>
              </li>
            </ul>
          </div>
        </nav> 
        {/* -----------akhir navbar-------- */}
        
        
        
        {/* -----------jumbotron-------- */}
        <div className="jumbotron text-center" id="jumbo">
          <img src={profile} className="rounded-circle imeg"/>
          <h1 id="h1">Ibrahim Alfatih</h1>
          <p className='kanan'>Web Design</p>
        </div>
        {/* -----------akhir jumbotron-------- */}


        {/* -----------about-------- */}
        <section className="about" id="about">
          <div className="container">
            <div className="row">
              <div className="col-md-12 kiri mb-4">
                <h2 className="text-center">About Me</h2>
                <hr/>
              </div>
            </div>

            <div className="row">
              <div className="col-md-12">
                <p className="kanan">&nbsp;&nbsp;Nama gw Ibrahim Alfatih atau panggil aja fatih. Gw lahir di Tegal tapi ga bisa bahasa Jawa 
                  karena di Tegal cuma numpang lahir aja :v, kalau besarnya sih di JawaBarat tepatnya yaitu di kota Bekasi. <br/>
                  &nbsp;&nbsp;Kalau gw sih hobynya noton film sama baca buku, Apalagi buku-buku yang berbau konspirasi  
                  pasti betah banget tuh duduk lama-lama buat baca buku, Dan kalau nonton filmnya sih gw lebih suka yang 
                  bergenre action. <br/> 
                  &nbsp;&nbsp;Dan kegiatan gw sekarang sih sebagai seorang Web Developer tepatnya di bagian Front-End tapi 
                  masih noob :v, Karena gw baru mulai belajar. <br/>
                  &nbsp;&nbsp;Adapun tempat gw mempelajari tentang Web Developer yaitu di Pondok IT tepatnya di Yogyakarta. Tidak seperti
                  kebanyakan pondok, Di Pondok IT ini selain kita mempelajari ilmu agama, kita juga mempelajari ilmu-ilmu teknologi,
                  seperti Web, Android, hingga Game Development.
                </p>
              </div>
            </div>
          </div>
        </section>
        {/* -----------akhir about-------- */}
        

        {/* -----------galery-------- */}
        <section className="gallery" id="gallery">
          <div className="container">
            <div className="row">
              <div className="col-md-12 mb-4">
                <h2 className="text-center">Gallery</h2>
                <hr/>
              </div>
            </div>
            <div className="row text-center"> 
                  <div className="col-md-4">
                    <a href="" className="img-thumbnail">
                      <img src={bromo} className='imeg'/>
                    </a>
                  </div>
                  <div className="col-md-4">
                    <a href="" className="img-thumbnail">
                      <img src={gambar} className='imeg'/>
                    </a>
                  </div>
                  <div className="col-md-4">
                    <a href="" className="img-thumbnail">
                      <img src={pantai} className='imeg'/>
                    </a>
                  </div>
            </div>
                  <br/>
            <div className="row text-center">
                  <div className="col-md-4">
                    <a href="" className="img-thumbnail">
                      <img src={hp} className='imeg'/>
                    </a>
                  </div>
                  <div className="col-md-4">
                    <a href="" className="img-thumbnail">
                      <img src={fatih} className='imeg'/>
                    </a>
                  </div>
                  <div className="col-md-4">
                    <a href="" className="img-thumbnail">
                      <img src={job} className='imeg'/>
                    </a>
                  </div>
            </div>
          </div>
        </section>
        {/* -----------akhir galery-------- */}
        
        
        
        {/* -----------contact-------- */}
        <section class="contact" id="contact">
          <div class="container">
            <div class="row">
              <div class="col">
                <h2 class="text-center">Contact Us</h2>
                <hr/>
              </div>
            </div>

            <div class="row justify-content-center">
              <div class="col-md-4">
                <div class="card text-white mb-3 text-center">
                  <div class="card-body">
                    <h4 class="card-title">Contact Us</h4>
                    <p class="card-text kanan">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                  </div>
                </div>
                <div class="list-group">
                  <h1 href="#" class="list-group-item list-group-item-action">Location</h1>
                  <a href="#" class="list-group-item list-group-item-action">My School</a>
                  <a href="#" class="list-group-item list-group-item-action">Yogyakarta</a>
                  <a href="#" class="list-group-item list-group-item-action">Pondok IT</a>
                </div>
              </div>

              <div class="col-md-6">
                <form>
                  <div class="form-group">
                    <label for="nama">Nama</label>
                    <input type="text" class="form-control" id="nama"/>
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" class="form-control" id="email"/>
                  </div>
                  <div class="form-group">
                    <label for="tlp">No.Telphone</label>
                    <input type="text" class="form-control" id="tlp"/>
                  </div>
                  <div class="form-group">
                    <label for="pesan">Pesan</label>
                    <textarea name="pesan" class="form-control" id="pesan"></textarea>
                  </div>
                  <button class="btn btn-dark tombol" type="submit">Kirim Pesan</button>
                </form>
              </div>
            </div>
          </div>
        </section>
        {/* -----------akhir contact-------- */}
        
        {/* -----------footer-------- */}
        <footer className='foot'>
          <div class="container text-center">
            <div class="row">
              <div class="col-md-12">
                <p className='kanan'>&copy; copyright 2018 | built by. Ibrahim Alfatih</p>
              </div>
            </div>
          </div>
        </footer>
        {/* -----------akhir footer-------- */}
      </div>
    );
  }
}

export default Porto;
