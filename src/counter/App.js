import React, { Component } from 'react';
import './bootstrap.min.css';
import './App.css';


class App extends Component {
  render() {
    return (
      <div className="aap">
        <Counter angka= "Bacalah istighfar 100x"/>
      </div>
    );
  }
}
class Counter extends Component{
  state = {
    number : 0
  }
  add = () =>{
    this.setState({
      number : this.state.number + 1
    })
  }
  sub = () =>{
    if(this.state.number <= 0){
      alert("MAAF tidak bisa kurang dari NOL")
    }else{
      this.setState({
        number : this.state.number - 1              
      })
    }
  }
  render(){
    return (
      <div className='text-center nomer'>
        <h1 id="hh1">{this.props.angka}</h1>
        <br/><br/><br/><br/><br/>
        <h2 className="display-1 angkak ">{this.state.number}</h2>
        <p className="lead">
        <button className='btn ukur' id="hitam" onClick={this.add}>ADD</button><button className="btn btn-drak ukur" onClick={this.sub}>SUBSTRACT</button>
        </p>
        <br/><br/><br/><br/><br/><br/><br/><br/><br/><br/><br/>
      </div>
    ) //kegunaan dari props biar bisa di pakai berulang ulang
  }
}

export default App;
