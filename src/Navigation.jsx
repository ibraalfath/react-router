import React, { Component } from 'react';
import {Link} from 'react-router-dom';
import './bootstrap.min.css';

class Navigation extends Component {
    render() {
        return (
            <div className="navigasi navbar-expand-lg text-center fixed-bottom">
                <Link to="/" className="ling btn btn-outline-secondary">About Me</Link> &nbsp;
                <Link to="/contact" className="ling btn btn-outline-secondary">Bootstrap</Link> &nbsp;
                <Link to="/about" className="ling btn btn-outline-secondary">Dzikir</Link>
            </div>
        );
    }
}

export default Navigation;