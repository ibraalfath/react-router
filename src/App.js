import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {BrowserRouter, Route, Switch} from 'react-router-dom'
import Home from './portofolio/Porto';
import Contact from './bootsrap/App';
import About from './counter/App';
import Navigation from './Navigation.jsx';

class App extends Component {
  render() {
    return (
      <div className="App">
        <BrowserRouter>
          <div>
          <div>
            <Navigation/>
          </div>
          <div>
            <Switch>
              <Route path="/" component={Home} exact/>
              <Route path="/contact" component={Contact}/>
              <Route path="/about" component={About}/>
            </Switch>
          </div>
          <div>
            <Navigation/>
          </div>
          </div>
        </BrowserRouter>
      </div>
    );
  }
}

export default App;
